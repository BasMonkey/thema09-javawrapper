# Thema09 JavaWrapper

This is a Java program that wraps my RandomForest model.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Requirements:
-Java 10
-Weka
-And this repository

You can download this repo by running the following command in a terminal:

```
$ git clone https://BasMonkey@bitbucket.org/BasMonkey/thema09-javawrapper.git
```

### Running

1) Open the repo project you just cloned as project in JetBrains' IntelliJ IDEA

2) Run the build.gradle

3) Lean back

4) Run the main function in ```CommandLineRunner.java``` while giving the right arguments

The input arguments should be as following:
    
```-d data/dia_data_small.arff -u data/dia_data_small_unknown.arff -m data/RandomForest.model```

The arguments explained:

* ```-d datafile``` where datafile is the path to the datafile with the known data in a *.arff file.

* ```-u unknowndatafile``` where unknowndatafile is the path to the datafile with the unknown data in a *.arff file.

* ```-m modelfile``` where modelfile is the path to the modelfile.

To get an idea of what each file should look like, take a look at the [data folder](https://bitbucket.org/BasMonkey/thema09-javawrapper/src/master/data/)

5) Do a dance

## Built With

* [Love](https://en.wikipedia.org/wiki/Love) - Love
* [CLI and Gradle demo](https://bitbucket.org/minoba/clidemo/) - CLI and Gradle demo
* [WEKA demo code](https://bitbucket.org/minoba/wekaapidemo/) - WEKA demo code

## Authors

* **Bas Kasemir** - *Initial work* - [Hanze University of Applied Sciences](https://bioinf.nl/~bhnkasemir)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
