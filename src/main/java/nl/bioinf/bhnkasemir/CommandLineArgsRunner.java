/*
 * Copyright (c) 2015 Michiel Noback
 * All rights reserved
 * www.bioinf.nl, www.cellingo.net
 */
package nl.bioinf.bhnkasemir;

import java.io.File;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Main class designed to work with user input provided standard CL arguments and parsed using Apache CLI. Class is
 * final because it is not designed for extension.
 *
 * @author michiel
 */
public final class CommandLineArgsRunner {

    /**
     * private constructor because this class is not supposed to be instantiated.
     */
    private CommandLineArgsRunner() {
    }

    /**
     * function that parses the args or gives a error message
     * @param args the command line arguments
     */
    public static void main(final String[] args) {
        try {
            ApacheCliOptionsProvider op = new ApacheCliOptionsProvider(args);
            if (op.helpRequested()) {
                op.printHelp();
                return;
            }

            WekaWrapper wrapper = new WekaWrapper();
            wrapper.start(op);
        } catch (IllegalStateException ex) {
            System.err.println("Something went wrong while processing your command line \""
                    + Arrays.toString(args) + "\"");
            System.err.println("Parsing failed.  Reason: " + ex.getMessage());
            ApacheCliOptionsProvider op = new ApacheCliOptionsProvider(new String[]{});
            op.printHelp();
        }
    }
}
