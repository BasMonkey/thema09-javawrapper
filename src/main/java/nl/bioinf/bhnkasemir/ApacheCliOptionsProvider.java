/*
 * Copyright (c) 2018 Bas Kasemir
 * All rights reserved
 * www.bioinf.nl/~bhnkasemir
/*

 */
package nl.bioinf.bhnkasemir;

import org.apache.commons.cli.*;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class implements the OptionsProvider interface by parsing the passed command line arguments.
 *
 * @author michiel
 */
public class ApacheCliOptionsProvider {
    private static final String HELP = "help";
    private static final String DATAFILE = "datafile";
    private static final String UNKNOWNFILE = "unknownfile";
    private static final String MODEL = "model";

    private final String[] clArguments;
    private Options options;
    private CommandLine commandLine;
    private String data;
    private String unknown;
    private String model;

    /**
     * constructs with the command line array.
     *
     * @param args the CL array
     */
    public ApacheCliOptionsProvider(final String[] args) {
        this.clArguments = args;
        initialize();
    }

    /**
     * Options initialization and processing.
     */
    private void initialize() {
        buildOptions();
        processCommandLine();
    }

    /**
     * check if help was requested; if so, return true.
     * @return helpRequested
     */
    public boolean helpRequested() {
        return this.commandLine.hasOption(HELP);
    }

    /**
     * builds the Options object.
     */
    private void buildOptions() {
        // create Options object
        this.options = new Options();
        Option helpOption = new Option("h", HELP, false, "Prints this message");
        Option dataOption = new Option("d", DATAFILE, true, "The *.arff file with the origninal dataset to be used by the algortihm");
        Option unknownfileOption = new Option("u", UNKNOWNFILE, true, "The *.arff file with the dataset to check");
        Option modelOption = new Option("m", MODEL, true, "Het *.model bestand");
        options.addOption(helpOption);
        options.addOption(dataOption);
        options.addOption(unknownfileOption);
        options.addOption(modelOption);
    }

    /**
     * processes the command line arguments.
     */
    private void processCommandLine() {
        try {
            CommandLineParser parser = new DefaultParser();
            this.commandLine = parser.parse(this.options, this.clArguments);

            if (commandLine.hasOption(HELP)) {
                return;
            }

            // check if all arguments are given
            if (!commandLine.hasOption(DATAFILE) || !commandLine.hasOption(UNKNOWNFILE) || !commandLine.hasOption(MODEL)) {
                exitError("Missing some arguments. Make sure to provide all 3 (three)");
            }

            String arff_file = "(.arff)$";
            String model_file = "(.model)$";

            // Create a Pattern object
            Pattern arffr = Pattern.compile(arff_file);
            Pattern modelr = Pattern.compile(model_file);

            // Now create matcher object.
            Matcher dr = arffr.matcher(this.commandLine.getOptionValue(DATAFILE));
            Matcher ur = arffr.matcher(this.commandLine.getOptionValue(UNKNOWNFILE));
            Matcher mr = modelr.matcher(this.commandLine.getOptionValue(MODEL));
            if (!dr.find( )) {
                exitError("The datafile argument does not have a valid file extension. An *.arff file is required.");
            } else if (!ur.find( )) {
                exitError("The unknown argument does not have a valid file extension. An *.arff file is required.");
            } else if (!mr.find( )) {
                exitError("The model argument does not have a valid file extension. An *.model file is required.");
            }

            File df = new File(this.commandLine.getOptionValue(DATAFILE));
            File uf = new File(this.commandLine.getOptionValue(UNKNOWNFILE));
            File mf = new File(this.commandLine.getOptionValue(MODEL));

            if (!df.exists() || df.isDirectory()) {
                exitError("Datafile does not exist or is a directory");
            } else if(!uf.exists() || uf.isDirectory()) {
                exitError("Datafile does not exist or is a directory");
            } else if(!mf.exists() || mf.isDirectory()) {
                exitError("Datafile does not exist or is a directory");
            }

            String da = this.commandLine.getOptionValue(DATAFILE);
            this.data = da;

            String uk = this.commandLine.getOptionValue(UNKNOWNFILE);
            this.unknown = uk;

            String mo = this.commandLine.getOptionValue(MODEL);
            this.model = mo;

        } catch (ParseException ex) {
            exitError("Illegal argument. Only -d -h -u -m arguments accepted");
        }
    }

    public void exitError(String msg) {
        System.out.println("/!\\ ERROR");
        System.out.println(msg);
        System.exit(1);
    }

    /**
     * prints help.
     */
    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("Weka Wrapper", options);
    }

    public String getDatafile() {
        return this.commandLine.getOptionValue(DATAFILE);
    }

    public String GetUnknownfile() {
        return this.commandLine.getOptionValue(UNKNOWNFILE);
    }

    public String getModel() {
        return this.commandLine.getOptionValue(MODEL);
    }

}
