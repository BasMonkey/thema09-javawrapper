package nl.bioinf.bhnkasemir;

import weka.classifiers.trees.RandomForest;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.ConverterUtils.DataSource;

import java.io.File;
import java.io.IOException;

public class WekaWrapper {
    //private final String modelFile = "data/RandomForest.model";

    /**
     * Calls the start of the program
     * @param args arguments given
     */
    public static void main(String[] args) {
        WekaWrapper wrapper = new WekaWrapper();
        ApacheCliOptionsProvider op = new ApacheCliOptionsProvider(args);
        wrapper.start(op);
    }


    /**
     * This function does the main logic of the program.
     * @param op An ApacheCliOptionsProvider object
     */

    public void start(ApacheCliOptionsProvider op) {
        try {

            String datafile = op.getDatafile();
            String unknownFile = op.GetUnknownfile();
            String modelFilepath = op.getModel();

            System.out.println("\nModel filepath : " + modelFilepath);
            System.out.println("unknownFile : " + unknownFile);
            System.out.println("datafile : " + datafile);

            System.out.println("\nRunning...\nPlease wait...\n\n");
            String modelFile = modelFilepath;
            Instances instances = loadArff(datafile);
            printInstances(instances);
            RandomForest randomForest = buildClassifier(instances);
            saveClassifier(randomForest, modelFile);
            RandomForest fromFile = loadClassifier(modelFile);
            Instances unknownInstances = loadArff(unknownFile);
            //System.out.println("\nunclassified unknownInstances = \n" + unknownInstances);
            classifyNewInstance(fromFile, unknownInstances);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Classify new instances
     * @param tree RandomForest Object
     * @param unknownInstances Instances object containing the unknown instances
     * @throws Exception
     */

    private void classifyNewInstance(RandomForest tree, Instances unknownInstances) throws Exception {
        System.out.println("\n\nAlmost there, classifying NewInstance\n");
        // create copy
        Instances labeled = new Instances(unknownInstances);
        // label instances
        for (int i = 0; i < unknownInstances.numInstances(); i++) {
            double clsLabel = tree.classifyInstance(unknownInstances.instance(i));
            labeled.instance(i).setClassValue(clsLabel);
        }
        //System.out.println("\nNew, labeled = \n" + labeled);

        System.out.println("\n\n#########\n# DONE #\n#########\n");
        // RESUOLTAAT IS labeled
        // Print naar een file

        ArffSaver saver = new ArffSaver();
        saver.setInstances(labeled);
        String pathname = "./data/results.arff";
        saver.setFile(new File( pathname));
        saver.writeBatch();

        System.out.println("Output results file is saved to " + pathname);
    }

    /**
     * Function that loads classifier from Weka
     * @param modelFile String containing the path to the *.model file
     * @return RandomForest model from the file
     * @throws Exception
     */
    private RandomForest loadClassifier(String modelFile) throws Exception {
        // deserialize model
        return (RandomForest) weka.core.SerializationHelper.read(modelFile);
    }

    /**
     * Function that saves the classifier
     * @param randomForest RandomForest Object
     * @param modelFile String containing the path to the *.model file
     * @throws Exception
     */
    private void saveClassifier(RandomForest randomForest, String modelFile) throws Exception {
        //post 3.5.5
        // serialize model
        weka.core.SerializationHelper.write(modelFile, randomForest);

        // serialize model pre 3.5.5
//        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(modelFile));
//        oos.writeObject(j48);
//        oos.flush();
//        oos.close();
    }

    /**
     * Function that builds the classifier
     * @param instances Instances object that are needed to build the model
     * @return Finalized model
     * @throws Exception
     */
    private RandomForest buildClassifier(Instances instances) throws Exception {
        System.out.println("\n\nStarting the real work, building a classifier\nPlease wait...\n\n");
        String[] options = new String[1];
        options[0] = "-U";            // unpruned tree
        RandomForest tree = new RandomForest();         // new instance of tree
        tree.setMaxDepth(0);
        tree.setBreakTiesRandomly(false);
        //tree.setOptions(options);     // set the options
        tree.buildClassifier(instances);   // build classifier
        return tree;

    }

    /**
     * Function that prints the instances to the command line
     * @param instances Instances object
     * @throws Exception
     */
    private void printInstances(Instances instances) throws Exception {
        int numAttributes = instances.numAttributes();

        for (int i = 0; i < numAttributes; i++) {
            System.out.println("attribute " + i + " = " + instances.attribute(i));
        }

        System.out.println("class index = " + instances.classIndex());
//        Enumeration<Instance> instanceEnumeration = instances.enumerateInstances();
//        while (instanceEnumeration.hasMoreElements()) {
//            Instance instance = instanceEnumeration.nextElement();
//            System.out.println("instance " + instance. + " = " + instance);
//        }

        //or
        /*int numInstances = instances.numInstances();
        for (int i = 0; i < numInstances; i++) {
            if (i == 5) break;
            Instance instance = instances.instance(i);
            System.out.println("instance = " + instance);
        }*/
    }

    /**
     * Function that reads the data from the *.arff file
     * @param datafile String containing path to the *.arff datafile
     * @return The datat is in the given *.arff file
     * @throws IOException
     */
    private Instances loadArff(String datafile) throws IOException {
        try {
            DataSource source = new DataSource(datafile);
            Instances data = source.getDataSet();
            // setting class attribute if the data format does not provide this information
            // For example, the XRFF format saves the class attribute information as well
            if (data.classIndex() == -1)
                data.setClassIndex(data.numAttributes() - 1);
            return data;
        } catch (Exception e) {
            throw new IOException("Could not read from file");
        }
    }


}
